# Brain Blox Balance Board Interface #

This software, developed initially in the Department of Integrative Physiology at University of Colorado Boulder, provides an interface to capture, record, and visualize data provide by the Wii Balance Board.  The current interface only works on Windows.  

*To connect the balance board or wiimotes:*
1. Make sure your computer has a working bluetooth transceiver or dongle and that the wii equipment has batteries
2. Press the 'connect' button on the wii equipment (located beneath the battery panel), the light will flash blue
3. Open 'My Bluetooth Places' and select 'Add a device'
4. Follow the prompts to add the wii device, the balance board appears as 'Nintendo-RLV-WBC-01'
5. The wii device should be connected and you can now open the BrainBLOX.exe executable

*Using the BrainBLOX software*
Upon opening the BrainBLOX software you will see the screen below, whose main features are described.

![screencapture1.JPG](https://bitbucket.org/repo/XjzM8G/images/641697139-screencapture1.JPG)

![screencapture2.JPG](https://bitbucket.org/repo/XjzM8G/images/2569945223-screencapture2.JPG)



### How do I get set up? ###

* Summary of set up

The binary package in the downloads section should have a single executable and a bunch of dll's.  After unzipping the files into a single folder, it should be sufficient to simply run the executable.  The software will search for a local bluetooth radio and complain if it can't find one.  Additional information is available on the wiki.

* Configuration and dependencies

Modifying and rebuilding the code is an involved process that will hopefully be simplified in the future.  This project is built around the Qt 5.3 framework.  It uses the compiler provided with Microsoft Visual Studio (initially developed with vs2010) augmented with the Windows Driver Kit (initially developed using WDK 7.1.0).  

Basic visualization and plotting make use of the [QCustomPlot class](http://www.qcustomplot.com/) by Emanuel Eichhammer.  Direct interaction with the Wii devices is made possible through a modified version of [the WiiUse library](https://github.com/rpavlik/wiiuse).  The QCustomPlot and modified WiiUse code are included with the source distribution (entire project is licensed under GPL v.3).  Visual Studio, the WDK, and Qt should be installed to compile this code.

Visual Studio can be obtained (free for students with a .edu email address) through [Microsoft Dreamspark](https://www.dreamspark.com/).  Install this first.

The Windows Driver Kit provides access to the BlueTooth libraries.  It is [available through Microsoft](http://www.microsoft.com/en-us/download/details.aspx?id=11800). **Note:** There is a bug in one of the header files that we use.  After installing the WDK, open the file `{WDKDir}\inc\api\bluetoothAPIs.h` and add the CALLBACK macro to the definition of PFN_AUTHENTICATION_CALLBACK_EX (line 1328 in my copy):  

```
#!c++
typedef BOOL (*PFN_AUTHENTICATION_CALLBACK_EX)(__in_opt LPVOID pvParam, __in PBLUETOOTH_AUTHENTICATION_CALLBACK_PARAMS pAuthCallbackParams);
```
becomes
```
#!c++
typedef BOOL (CALLBACK *PFN_AUTHENTICATION_CALLBACK_EX)(__in_opt LPVOID pvParam, __in PBLUETOOTH_AUTHENTICATION_CALLBACK_PARAMS pAuthCallbackParams);
```

Finally, Qt Creator provides a convenient IDE for working with Qt projects.  [Download and install Qt Creator from here](http://qt-project.org/downloads).

* Build

If Qt is correctly configured, you should be able to open the WiiUse and BalanceBoard projects.  At the moment, it is necessary to modify the .pro files because they use absolute paths to find each other and the WDK.  This should be fixed in the future.

* Deployment instructions

Recent version of Qt provide a Deployment executable that wraps up all the binary files needed to support a Qt project.  I accomplish this by building in Release mode, copying the executable to a new folder, and then using the command line to run `windeployqt.exe`  (improved instructions pending).