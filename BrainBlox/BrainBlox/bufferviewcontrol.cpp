#include "bufferviewcontrol.h"
#include "ui_bufferviewcontrol.h"

#include <QColorDialog>
#include <QFileDialog>

#include "wiidevice.h"

BufferViewControl::BufferViewControl(WiiDevice* device, PlotSceneView* rView, PlotSceneView* fView, QCustomPlot* tView, QWidget *parent) :
  QWidget(parent),
  ui(new Ui::BufferViewControl),
  lastX(0), lastY(0),
  recentView(nullptr),
  fullView(nullptr),
  timeView(nullptr),
  recording(false),
  recentActive(false),
  fullActive(false),
  timeActive(false),
  recentFrame(0),
  fullFrame(0),
  timeFrame(0)
{
  ui->setupUi(this);
  attachDevice(device);
  setViews(rView,fView,tView);

  data.reserve(250000);



  // ***** todo:
  // Ideally, we'll come up with a way
  // to identify individual devices and keep
  // them associated with the parameters chosen for
  // them with a .ini file so that you don't
  // have to change the color every time
  // you run the program.

  // Some default colors for now.

  setCurrentColor(QColor(Qt::green));
  setOldColor(QColor(Qt::white));
  setRecentColor(QColor(Qt::blue));
  setFullColor(QColor(Qt::blue));
  setXColor(QColor(Qt::blue));
  setYColor(QColor(Qt::green));
  setFColor(QColor(Qt::black));

  changeXWidth(ui->xPixelSpinBox->value());
  changeYWidth(ui->yPixelSpinBox->value());
  changeFWidth(ui->fPixelSpinBox->value());


  // Connect the buttons and other elements to their respective functions.

  connect(ui->currentColorButton,SIGNAL(clicked()),this,SLOT(currentColorDialog()));
  connect(ui->recentColorButton,SIGNAL(clicked()),this,SLOT(recentColorDialog()));
  connect(ui->oldColorButton,SIGNAL(clicked()),this,SLOT(oldColorDialog()));
  connect(ui->fullColorButton,SIGNAL(clicked()),this,SLOT(fullColorDialog()));
  connect(ui->xColorButton,SIGNAL(clicked()),this,SLOT(xColorDialog()));
  connect(ui->yColorButton,SIGNAL(clicked()),this,SLOT(yColorDialog()));
  connect(ui->fColorButton,SIGNAL(clicked()),this,SLOT(fColorDialog()));

  connect(ui->historyFramesSpinBox,SIGNAL(valueChanged(int)),
          this,SLOT(setHistoryLength(int)));
  setHistoryLength(ui->historyFramesSpinBox->value());

  connect(ui->xPixelSpinBox,SIGNAL(valueChanged(int)),this,SLOT(changeXWidth(int)));
  connect(ui->yPixelSpinBox,SIGNAL(valueChanged(int)),this,SLOT(changeYWidth(int)));
  connect(ui->fPixelSpinBox,SIGNAL(valueChanged(int)),this,SLOT(changeFWidth(int)));

  // ***** We need to check to see if this is a real device
  // if we're going to allow files to be loaded.
  connect(ui->calibratePushButton,SIGNAL(clicked()),device,SLOT(zeroCalibration()));


  connect(ui->recordPushButton,SIGNAL(clicked(bool)),this,SLOT(recordToggle(bool)));
  connect(ui->clearFramesButton,SIGNAL(clicked()),this,SLOT(clearBuffer()));
  connect(ui->savePushButton,SIGNAL(clicked()),this,SLOT(saveDialog()));

  connect(ui->recentPlotCheckBox,SIGNAL(toggled(bool)),this,SLOT(setRecentActive(bool)));



}

BufferViewControl::~BufferViewControl()
{
  delete ui;
}

void BufferViewControl::attachDevice(WiiDevice* device)
{
  if (device) {
    connect(device,SIGNAL(boardFrame(float,float,float,qint64)),
            this,SLOT(addFrame(float,float,float,qint64)));
    connect(device,SIGNAL(batteryLevel(float)),
            this,SLOT(setBatteryLevel(float)));
    connect(device,SIGNAL(buttonPress(WiiDevice::Button)),
            this,SLOT(checkButton()));
    connect(device,SIGNAL(buttonRelease(WiiDevice::Button)),
            this,SLOT(releaseButton()));
  }
}

void BufferViewControl::setViews(PlotSceneView* rView,PlotSceneView* fView,QCustomPlot* tView)
{
  recentView = rView;
  fullView = fView;
  timeView = tView;

  // Create a current view item
  currentItem = recentView->scene()->addEllipse(0,0,20,20,
                                                QPen(Qt::NoPen),QBrush(Qt::black));

  QRect vRect = recentView->viewport()->rect();
  connect(rView,SIGNAL(viewSize(int,int)),this,SLOT(setViewScale(int,int)));
  setViewScale(vRect.width(),vRect.height());

  // Create graphs in the time view
  xGraph = timeView->addGraph();
  yGraph = timeView->addGraph();
  fGraph = timeView->addGraph(timeView->xAxis,timeView->yAxis2);


}



/**
 * @brief BufferViewControl::saveData writes the data out to file
 * @param filename
 * @return true on sucess
 *
 * This needs to be made more friendly.
 */
bool BufferViewControl::saveData(const QString& filename)
{
  QFile file(filename);
  if (file.open(QFile::WriteOnly | QFile::Truncate)) {
    QTextStream out(&file);

    auto datEnd = data.end();
    for (auto datIt = data.begin();datIt!=datEnd;++datIt) {
      out << datIt->tStamp << " "
          << datIt->x << " " << datIt->y << " "
          << datIt->f << "\n";
    }
    return true;
  } else {
    return false;
  }

}

void BufferViewControl::setViewScale(int ww,int hh)
{
  int wScale = ww/50; // Deliberately use integer truncation
  int hScale = hh/30;
  double scaleVal = wScale<hScale?wScale:hScale;
  ui->recentScaleSpinBox->setValue(scaleVal);

  recentView->setSceneRect(-ww/2,-hh/2,ww,hh);
  recentView->centerOn(0,0);
}

void BufferViewControl::setBatteryLevel(float percent)
{
  ui->batteryProgressBar->setValue(percent*100);
}

void BufferViewControl::recordToggle(bool shouldRecord)
{
  recording = shouldRecord;
  ui->recordPushButton->setChecked(recording);
}

void BufferViewControl::clearBuffer()
{
  data.clear();

  xGraph->clearData();
  yGraph->clearData();
  fGraph->clearData();

  QGraphicsScene* scene = fullView->scene();
  size_t lSize = fullHistory.size();
  for (size_t ii=0;ii<lSize;++ii) {
    scene->removeItem(fullHistory[ii]);
    delete fullHistory[ii];
  }
  fullHistory.clear();

  recentFrame = fullFrame = timeFrame = 0;

}

void BufferViewControl::saveDialog()
{
  recordToggle(false);
  if (!data.empty()) {
    QString filename = QFileDialog::getSaveFileName(this,"Save filename");
    if (!filename.isEmpty()) {
      saveData(filename);
    }
  }
}

void BufferViewControl::checkButton()
{
  ui->aPushButton->setChecked(true);
}

void BufferViewControl::releaseButton()
{
  ui->aPushButton->setChecked(false);
}

void BufferViewControl::setRecentActive(bool isActive)
{
  recentActive = isActive;
  currentItem->setVisible(isActive);
}

void BufferViewControl::setTimeActive(bool isActive)
{

  timeActive = isActive;
}

void BufferViewControl::setFullActive(bool isActive)
{
  fullActive = isActive;
}

void BufferViewControl::showXTPlot(bool isSet)
{
  xGraph->setVisible(isSet);
}

void BufferViewControl::showYTPlot(bool isSet)
{
  yGraph->setVisible(isSet);
}

void BufferViewControl::showFTPlot(bool isSet)
{
  fGraph->setVisible(isSet);
}

void BufferViewControl::changeXWidth(int pix)
{
  xGraph->setPen(QPen(QBrush(xColor),pix));
}

void BufferViewControl::changeYWidth(int pix)
{
  yGraph->setPen(QPen(QBrush(yColor),pix));
}

void BufferViewControl::changeFWidth(int pix)
{
  fGraph->setPen(QPen(QBrush(fColor),pix));
}

void BufferViewControl::setHistoryLength(int length)
{
  QGraphicsScene* scene = recentView->scene();

  if (length<0) length = 0;
  size_t len = (size_t) length;
  size_t oldSize = recentHistory.size();

  if (oldSize>len) {
    // Clean up the extra items
    for (size_t ii = len;ii<oldSize;++ii) {
     QGraphicsEllipseItem* item = recentHistory[ii];
     scene->removeItem(item);
     delete item;
    }
    recentHistory.resize(len);
  } else if (oldSize<len) {
    // Add in some new frames with the
    // same shape/position as the last item
    QGraphicsEllipseItem* item = currentItem;
    if (oldSize) {
     item = recentHistory[oldSize-1];
    }
    recentHistory.resize(len);
    for (size_t ii=oldSize;ii<len;++ii) {
     recentHistory[ii] = scene->addEllipse(item->rect(),QPen(Qt::NoPen));
     recentHistory[ii]->setZValue(-double(ii)-1);
    }
  }
  // Set the colors
  interpolateColors();
}


void BufferViewControl::currentColorDialog()
{
  setCurrentColor(QColorDialog::getColor(currentColor,this));
}

void BufferViewControl::recentColorDialog()
{
  setRecentColor(QColorDialog::getColor(recentColor,this));
}
void BufferViewControl::oldColorDialog()
{
  setOldColor(QColorDialog::getColor(oldColor,this));
}

void BufferViewControl::fullColorDialog()
{
  setFullColor(QColorDialog::getColor(fullColor,this));
}

void BufferViewControl::xColorDialog()
{
  setXColor(QColorDialog::getColor(xColor,this));
}
void BufferViewControl::yColorDialog()
{
  setYColor(QColorDialog::getColor(yColor,this));
}

void BufferViewControl::fColorDialog()
{
  setFColor(QColorDialog::getColor(fColor,this));
}

void BufferViewControl::setCurrentColor(const QColor& color)
{
  if (color.isValid()) {
    currentColor = color;
    setToolButtonColor(ui->currentColorButton,color);
    QBrush brush = currentItem->brush();
    brush.setColor(color);
    currentItem->setBrush(brush);
  }
}

void BufferViewControl::setOldColor(const QColor& color)
{
  if (color.isValid()) {
    oldColor = color;
    setToolButtonColor(ui->oldColorButton,color);
    //if (recentView) recentView->setOldColor(color);
    interpolateColors();
  }
}

void BufferViewControl::setRecentColor(const QColor& color)
{
  if (color.isValid()) {
    recentColor = color;
    setToolButtonColor(ui->recentColorButton,color);
    //if (recentView) recentView->setRecentColor(color);
    interpolateColors();
  }
}

void BufferViewControl::setFullColor(const QColor& color)
{
  if (color.isValid()) {
    fullColor = color;
    setToolButtonColor(ui->fullColorButton,color);
    //if (fullView) fullView->setRecentColor(color);
  }
}

void BufferViewControl::setXColor(const QColor& color)
{
  if (color.isValid()) {
    xColor = color;
    setToolButtonColor(ui->xColorButton,color);
    QPen pen = xGraph->pen();
    pen.setColor(color);
    xGraph->setPen(pen);
  }
}

void BufferViewControl::setYColor(const QColor& color)
{
  if (color.isValid()) {
    yColor = color;
    setToolButtonColor(ui->yColorButton,color);
    QPen pen = yGraph->pen();
    pen.setColor(color);
    yGraph->setPen(pen);
  }
}

void BufferViewControl::setFColor(const QColor& color)
{
  if (color.isValid()) {
    fColor = color;
    setToolButtonColor(ui->fColorButton,color);
    QPen pen = fGraph->pen();
    pen.setColor(color);
    fGraph->setPen(pen);
  }
}

/**
 * @brief BufferViewControl::addFrame records this data point
 * @param x
 * @param y
 * @param f
 * @param tStamp
 *
 * Set this data point as the current set of values.  If we're
 * recording data, also push the data point onto our buffer.
 *
 * Visualization will be taken care of by a different function.
 */
void BufferViewControl::addFrame(float x,float y,float f,qint64 tStamp)
{

  //currentItem->setRect(10*x,10*y,f,f);
  mostRecent.x=x;
  mostRecent.y=y;
  mostRecent.f=f;
  mostRecent.tStamp=tStamp;

  // If we're recording, push this onto the data vector.
  if (recording) {
    size_t kk = data.size()+1;
    if (kk==1) {
      minX=maxX=meanX=x; varX = 0;
      minY=maxY=meanY=y; varY = 0;
      minF=maxF=meanF=f; varF = 0;
    } else {
      float oldX = meanX;
      float oldY = meanY;
      float oldF = meanF;

      if (x<minX) minX=x;
      if (x>maxX) maxX=x;
      if (y<minY) minY=y;
      if (y>maxY) maxY=y;
      if (f<minF) minF=f;
      if (f>maxF) maxF=f;

      meanX = oldX + (x-oldX)/kk;
      varX  += (x-oldX)*(x-meanX);
      meanY = oldY + (y-oldY)/kk;
      varY  += (y-oldY)*(y-meanY);
      meanF = oldF + (f-oldF)/kk;
      varF  += (f-oldF)*(f-meanF);
    }
    data.push_back(mostRecent);



    ui->frameCountLabel->setText(QString::number(kk));
    ui->minXLabel->setText(QString::number(minX,'f',2));
    ui->maxXLabel->setText(QString::number(maxX,'f',2));
    ui->meanXLabel->setText(QString::number(meanX,'f',2));
    ui->varXLabel->setText(QString::number(varX/kk,'f',2));

    ui->minYLabel->setText(QString::number(minY,'f',2));
    ui->maxYLabel->setText(QString::number(maxY,'f',2));
    ui->meanYLabel->setText(QString::number(meanY,'f',2));
    ui->varYLabel->setText(QString::number(varY/kk,'f',2));

    ui->minFLabel->setText(QString::number(minF,'f',2));
    ui->maxFLabel->setText(QString::number(maxF,'f',2));
    ui->meanFLabel->setText(QString::number(meanF,'f',2));
    ui->varFLabel->setText(QString::number(varF/kk,'f',2));



  }


  //double key = tStamp/1000.0;

//  xGraph->addData(key,x);
//  yGraph->addData(key,y);
//  fGraph->addData(key,f);

//  xGraph->removeDataBefore(key-8);
//  yGraph->removeDataBefore(key-8);
//  fGraph->removeDataBefore(key-8);
//  // rescale value (vertical) axis to fit the current data:
////  xGraph->rescaleValueAxis();
////  yGraph->rescaleValueAxis(true);
////  fGraph->rescaleValueAxis(true);

//  //fullView->scene()->addLine(lastX,lastY,10*x,10*y);
//  lastX=10*x; lastY=10*y;

//  timeView->xAxis->setRange(key+0.25, 8, Qt::AlignRight);
//  timeView->replot();
}

/**
 * @brief BufferViewControl::updateViews checks which views are
 * currently visible.  It also keeps track of which data points
 * have been written to each view.  It then takes care of
 * writing whatever is new out to the view.
 */
void BufferViewControl::updateViews()
{
  size_t frames = data.size();

  // ////////////////////
  // Handle the recent view
  // ////////////////////
  if (recentView->isVisible() && ui->recentPlotCheckBox->isChecked()) {
    double scale = ui->recentScaleSpinBox->value();
    // Set the current view
    if (ui->proportionalCheckBox->isChecked()) {
      currentItem->setRect(scale*mostRecent.x-mostRecent.f/2,
                           scale*mostRecent.y-mostRecent.f/2,
                           mostRecent.f,mostRecent.f);
    } else {
      currentItem->setRect(scale*mostRecent.x-5,scale*mostRecent.y-5,
                           10,10);
    }

    if (!data.empty()) {
      // Set the history items
      // We'll assume that we've already created
      // the right number of GraphicsItems
      // they're the right color and depth.
      // But they need position and size;
      int recentIdx = 0;
      int hSize = recentHistory.size();
      int dataIdx = data.size()-1;
      for (;recentIdx<hSize && dataIdx>=0; recentIdx++,dataIdx--) {
        if (ui->proportionalCheckBox->isChecked()) {

          recentHistory[recentIdx]->setRect(
                scale*data[dataIdx].x-data[dataIdx].f/2,
                scale*data[dataIdx].y-data[dataIdx].f/2,
                data[dataIdx].f,data[dataIdx].f);
        } else {
          recentHistory[recentIdx]->setRect(
                scale*data[dataIdx].x-5,
                scale*data[dataIdx].y-5,
                10,10);
        }

      }
    }
  }

  // ////////////////////
  // Handle the full view
  // ////////////////////
  if (fullView->isVisible() && ui->fullPlotCheckBox->isChecked()) {
    QGraphicsScene* scene = fullView->scene();
    float lastX=0,lastY=0;

    if (fullFrame>0) {
      lastX = data[fullFrame-1].x;
      lastY = data[fullFrame-1].y;
    } else if (fullFrame<frames) {
      lastX = data[fullFrame].x;
      lastY = data[fullFrame].y;
      fullFrame+=1;
    }
    while (fullFrame<frames) {
      double scale = ui->fullScaleSpinBox->value();
      fullHistory.push_back(
            scene->addLine(lastX*scale,lastY*scale,
                           scale*data[fullFrame].x,scale*data[fullFrame].y,
                           QPen(fullColor)));

      lastX = data[fullFrame].x;
      lastY = data[fullFrame].y;
      fullFrame+=1;
    }
  }

  // ////////////////////
  // Handle the time view
  // ////////////////////
  if (timeView->isVisible() && ui->timePlotCheckBox->isChecked()) {
    double key = mostRecent.tStamp/1000.0;
    if (ui->xCheckBox->isChecked()) {
      xGraph->addData(key,mostRecent.x);
    }
    if (ui->yCheckBox->isChecked()) {
      yGraph->addData(key,mostRecent.y);

    }
    if (ui->fCheckBox->isChecked()) {
      fGraph->addData(key,mostRecent.f);
    }
    xGraph->removeDataBefore(key-8);
    yGraph->removeDataBefore(key-8);
    fGraph->removeDataBefore(key-8);
    timeView->xAxis->setRange(key+0.25, 8, Qt::AlignRight);
    timeView->replot();

  }

}

/**
 * @brief BufferViewControl::setToolButtonColor sets the background color of 'button' to
 * be color, using the style sheet.
 * @param button
 * @param color
 */
void BufferViewControl::setToolButtonColor(QToolButton* button,const QColor& color)
{
  if (color.isValid()) {
    button->setStyleSheet(
          QString("background-color: %1; ").arg(color.name()));
  }
}

/**
 * @brief BufferViewControl::interpolateColors sets the item color
 * for all of the recent history, interpolating between the recent
 * color to the old color
 */
void BufferViewControl::interpolateColors()
{
  if (recentHistory.empty()) return;
  size_t frames = recentHistory.size();

  if (frames==1) {
    recentHistory[0]->setBrush(QBrush(recentColor));
  } else {
    for (size_t ii=0;ii<frames;++ii) {
      double frac = ii/double(frames-1);
      recentHistory[ii]->setBrush(QBrush(colorMix(frac)));
    }
  }
}

/**
 * @brief BufferViewControl::colorMix does a linear interpolation between
 * the oldColor and recentColor
 * @param frac
 * @return
 */
QColor BufferViewControl::colorMix(double frac) const
{
  // Hue is negative whenever value or saturation
  // are zero.  It's bad to interpolate with
  // negative numbers; so we match the hue of
  // the non-negative, if one exists.
  qreal h1 = oldColor.hsvHueF();
  qreal h2 = recentColor.hsvHueF();
  if (h1<0 || h2<0) {
    if (h2>=0) h1=h2;
    else if (h1>=0) h2=h1;
    else h1=h2=0;
  }

  qreal h = h1*(frac) + h2*(1-frac);
  qreal s = oldColor.hsvSaturationF()*(frac) + recentColor.hsvSaturationF()*(1-frac);
  qreal v = oldColor.valueF()*(frac) + recentColor.valueF()*(1-frac);

  return QColor::fromHsvF(h,s,v);
}
