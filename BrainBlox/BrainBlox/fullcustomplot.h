#ifndef FULLCUSTOMPLOT_H
#define FULLCUSTOMPLOT_H

#include "qcustomplot.h"

class FullCustomPlot : public QCustomPlot
{
  Q_OBJECT
public:
  explicit FullCustomPlot(QWidget *parent = 0);

signals:

public slots:

protected:
  virtual void resizeEvent ( QResizeEvent * event );
  virtual void showEvent ( QShowEvent * event );

};

#endif // FULLCUSTOMPLOT_H
