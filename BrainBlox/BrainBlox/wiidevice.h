#ifndef WIIDEVICE_H
#define WIIDEVICE_H

#include <QObject>
#include "dataframe.h"

struct wiimote_t;

/**
 * @brief The WiiDevice class encapsulates a single device:
 *  Wiimote or Balance board.  For now, however, it's just
 *  a balance board.  We might do wiimotes later.
 *
 */
class WiiDevice : public QObject
{
  Q_OBJECT
public:
  /**
   * The buttons on the Wiimote.  We don't
   * get a signal from the Power button or Sync button.
   * The button on the balance board is ButtonA.
   */
  enum Button {
    ButtonOne,
    ButtonTwo,
    ButtonA,
    ButtonB,
    ButtonPlus,
    ButtonMinus,
    ButtonHome,
    ButtonUp,
    ButtonDown,
    ButtonLeft,
    ButtonRight,
    ButtonCount
  };

public:
  explicit WiiDevice(struct wiimote_t* dev,QObject *parent = 0);

  QString address(); ///< Get a unique identifier for the device
  bool isBalanceBoard(); ///< Is this a balance board
  // These functions only work for balance board
  const float* cornerForces(); ///< Get an array of four force values
  QPointF centerOfPressure();  ///< Get the computed center of pressure
  float totalForce();          ///< Get the current computed total force

  bool buttonIsDown(WiiDevice::Button button); ///< Is this button currently pressed?
  bool ledIsOn(int idx);       ///< Is this LED on?
  bool rumbleIsOn();           ///< Is the rumble on?

  void handleEvent();    ///< Buttons or streamed data (forces)

  void requestStatus();
  void handleStatus();   ///< Battery power (among other things)

  void poll();
  void processEvents();

signals:
  void buttonPress(WiiDevice::Button button);   ///< A button was just pressed
  void buttonRelease(WiiDevice::Button button); ///< A button was just released
  // Should we announce when internal data are updated?

  void boardFrame(float tl,float tr,float bl,float br,qint64 tStamp);
  void accelFrame(float ax,float ay,float az,qint64 tStamp);
  void batteryLevel(float level);

public slots:

  void zeroCalibration();


public:
  struct wiimote_t* wm;
  bool boardCheck;

  QString id; ///< The mac address


};

#endif // WIIDEVICE_H
