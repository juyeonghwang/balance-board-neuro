#-------------------------------------------------
#
# CU Neuromechanics BrainBlox project
#
# This project creates an interface for capturing,
# recording, and visualizing data from wiimotes and
# wii balance boards.  The intent is to provide a
# simple method for using these devices for simple
# demonstrations of physiological and psychological
# principles.
#
#-------------------------------------------------

QT       += core gui concurrent

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = BrainBlox
TEMPLATE = app
VERSION = 0.2.0

DEFINES += WIIUSE_STATIC

SOURCES += main.cpp\
        mainwindow.cpp \
    wiiusehandler.cpp \
    wiidevice.cpp \
    qcustomplot.cpp \
    plotsceneview.cpp \
    globaltime.cpp \
    pairingdialog.cpp \
    bluetoothinterface.cpp \
    bluetoothstructures.cpp \
    bufferscrollcontrol.cpp \
    fullcustomplot.cpp

HEADERS  += mainwindow.h \
    wiiusehandler.h \
    wiidevice.h \
    qcustomplot.h \
    plotsceneview.h \
    globaltime.h \
    pairingdialog.h \
    bluetoothinterface.h \
    bluetoothstructures.h \
    bufferscrollcontrol.h \
    fullcustomplot.h \
    dataframe.h

FORMS    += mainwindow.ui \
    pairingdialog.ui \
    bufferscrollcontrol.ui

# For WDK 7.1, we had to add CALLBACK to the signature of
# PFN_AUTHENTICATION_CALLBACK_EX in bluetoothAPIs.h to get
# the code to compile.  This is a known bug in the header file.
# It seems to have been repaired in WDK 8.1

# If the WDK is installed into an irregular location
# then you may need to specify the include and library paths
# for that.

# The include path is relative to the location of this
# project file.
INCLUDEPATH += ../wiiuse

# The linking path is relative to the parent directory
# of the output folder.  Very weird.

LIBS += ../lib/wiiuse.lib
win32:LIBS += Setupapi.lib
win32:LIBS += hid.lib
win32:LIBS += Bthprops.lib
win32:LIBS += Ws2_32.lib

PRE_TARGETDEPS += ../lib/wiiuse.lib

RESOURCES += \
    icons.qrc

