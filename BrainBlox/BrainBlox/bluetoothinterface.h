#ifndef BLUETOOTHINTERFACE_H
#define BLUETOOTHINTERFACE_H

#include <QObject>
#include <QtConcurrent>
#include <QWidget>
#include <Windows.h>
#include <map>

// Forward declare this class so that we don't have to
// include the bluetooth-specific headers everywhere.
class BluetoothStructures;

/**
 * @brief The BluetoothInterface class handles the Bluetooth
 * radio, connections, and communications.
 */
class BluetoothInterface : public QObject
{
  Q_OBJECT
public:
  // Create a BluetoothInterface object
  explicit BluetoothInterface(QObject *parent = 0);
  virtual ~BluetoothInterface();

signals:
  void debugString(QString);
  void finishedInquiry();

public slots:
  /// Spawn a thread to actively search for discoverable devices
  void beginInquiry();
  /// Recognize when the inquiry thread has finished
  void inquiryComplete();
  /// Recognize when an authentication thread is complete
  void authComplete();

  /// Initiate or terminate active pairing
  void pairing(bool toggle);

public:
  bool findBluetoothRadio();        ///< Locate local radios/dongles
  bool beginEventCapture(WId wid);  ///< Hook into the device events
  void closeEventCapture();         ///< Release device hook

  /// Respond to changes in bluetooth status
  bool deviceChange(MSG* msg,long* result);


  /// The inquiry thread blocks for a short time while querying
  /// for bluetooth devices
  void inquiry();


  /// Make a non-blocking call to retrieve information on known
  /// devices
  void queryBluetooth();

protected:

  // We can actually push all the windows-specific stuff
  // into the BluetoothStructures.  That will facilitate
  // making this cross-platform eventually.

  bool activeInquiry;      ///< Are we actively looking for bt info
  HANDLE radioHandle;      ///< The radio we're using
  HDEVNOTIFY notifyHandle; ///< Our link to receive bt device messages


  //std::map<unsigned long long,bool> addMap;

  HANDLE devHandle;
  bool wasDiscoverable;

  QFuture<void> inquiryFuture;
  QFutureWatcher<void> inquiryWatcher;

  QFuture<DWORD> authFuture;
  QFutureWatcher<DWORD> authWatcher;

  BluetoothStructures* bts;
};

#endif // BLUETOOTHINTERFACE_H
